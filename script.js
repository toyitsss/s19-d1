console.log('*** JS ES6 Updates ***');

/*
1. Exponent Operator
*/
//Before ES6
const firstNum = 8 ** 2;
console.log(firstNum);

//Update Es6
const secondNum = Math.pow(8, 2); //pow is power
console.log(secondNum);

/*
2. Template Literals (``) using backtics
-Allows to write string without using the concatenation operator (+)
-Greatly helps with code readability
*/
let name = "John";
//before
let message = 'Hello ' + name + '! Welcome to programming!';
console.log("message without template literals: " + message);

//using template literals
message = `Hello ${name}! Welcome to programming!`;
console.log(`message with template literals: ${message}`);

//multi-line - space between ` and $
const anotherMessage = `
${name} attended a Math competition. He won it by solving the problem 8 ** 2 with the  solution of ${firstNum}`
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings account is: ${principal * interestRate}`);

/*
3. Array Destructuring
-allows to unpack elements in arrays into distinct variables
-allows us to name array elements with variables instead of using index numbers
*/
const fullName = ["Juan", "Dela", "Cruz"];
//before
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

//USING DESTRUCTURING
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

/*
4. Object Destructuring
-allows to unpack properties of an object into distinct variables
*/
const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
};
//before
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

//USING DESTRUCTURING
const { givenName, maidenName, familyName } = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

/*
5. Arrow function
- compact alternative syntax to traditional functions
- useful for code snippets where creating functions will not be reused in any other portion of the code
- syntax:
const/let variableName = (parameters) => {statements};
*/
//Before
const hello	= function(){
	console.log("Hello world!");
}

//Using arrow function
const helloAgain = () => {
	console.log("Hello world!");
}
hello();
helloAgain();

//Arrow functions with loops
const students = ["John", "Jane", "Judy"];

//Before
students.forEach(function(students){
	console.log(`${students} is a student.`);
});

//using arrow function
students.forEach((students) => {
	console.log(`${students} is a student.`);
})

/*
6. Implicit Return Statement
- there are some instances when you can omit the "return statement"
-JS implicitly adds it for the result of the function
*/

// const add = (x, y) => {return x + y}; //object literal
const add = (x, y) => x + y; //implicit statement
let total = add(1, 2);
console.log(total);

/*
7. Default Function Argument Value
- provided a default argument value if none is provided
*/

const greet = (name = 'User') =>{
	return `Good morning, ${name}!`;
}
console.log(greet()); //output: Goodmorning User! ginamit ang default na "User"
console.log(greet("John")); //output: Goodmorning John! dahil nagdeclare ng bagong user.

/*
8. Class-Based Object Blueprint
-allows creation/instantiation of objects using classes as blueprints

Creating a class
-the constructor is a special method of a class for creating an object for that class
*/
class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.year = year;
		this.name = name;
	}
}

const myCar = new Car();
console.log(myCar)
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);

//Creating new instance of car with initialized values
const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);